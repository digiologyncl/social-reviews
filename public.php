<?php

require_once TUTSUSOCIALREVIEW_PLUGIN_DIR . '/public/enqueue.php';
require_once TUTSUSOCIALREVIEW_PLUGIN_DIR . '/public/yelp-reviews.php';
// require_once TUTSUSOCIALREVIEW_PLUGIN_DIR . '/public/facebook-reviews.php';
require_once TUTSUSOCIALREVIEW_PLUGIN_DIR . '/public/google-reviews.php';
require_once TUTSUSOCIALREVIEW_PLUGIN_DIR . '/public/rateabiz-reviews.php';
require_once TUTSUSOCIALREVIEW_PLUGIN_DIR . '/public/update-posts.php';
require_once TUTSUSOCIALREVIEW_PLUGIN_DIR . '/public/shortcodes.php';