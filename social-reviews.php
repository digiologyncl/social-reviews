<?php
/**
 * Plugin Name: Social Reviews
 * Plugin URI: -
 * Description: A TUTSU add-on to show Yelp, Google Plus and Rateabiz reviews.
 * Author: Marek Cich & Tom Ungerer
 * Author URI: http://digiology.tv/
 * Text Domain: tutsu_social_review
 * Version: 0.9.3.3
 * Bitbucket Plugin URI: Digiology/social-reviews
 * Bitbucket Plugin URI: https://bitbucket.org/Digiology/social-reviews
 */

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo 'Nah';
	exit;
}

define( 'TUTSUSOCIALREVIEW_VERSION', '0.9.3.3' );

define( 'TUTSUSOCIALREVIEW_REQUIRED_WP_VERSION', '4.7' );

define( 'TUTSUSOCIALREVIEW_PLUGIN', __FILE__ );

define( 'TUTSUSOCIALREVIEW_PLUGIN_BASENAME', plugin_basename( TUTSUSOCIALREVIEW_PLUGIN ) );

define( 'TUTSUSOCIALREVIEW_PLUGIN_NAME', trim( dirname( TUTSUSOCIALREVIEW_PLUGIN_BASENAME ), '/' ) );

define( 'TUTSUSOCIALREVIEW_PLUGIN_DIR', untrailingslashit( dirname( TUTSUSOCIALREVIEW_PLUGIN ) ) );


// ADMIN
require_once TUTSUSOCIALREVIEW_PLUGIN_DIR . '/admin.php';

// PUBLIC
require_once TUTSUSOCIALREVIEW_PLUGIN_DIR . '/public.php';


// PLUGIN URL
function sr_social_reviews_plugin_url( $path = '' ) {
  $url = plugins_url( $path, TUTSUSOCIALREVIEW_PLUGIN );
  if ( is_ssl() && 'http:' == substr( $url, 0, 5 ) ) {
    $url = 'https:' . substr( $url, 5 );
  }
  return $url;
}


// DISPLAY ERRORS
function sr_admin_notice__error($text) {
  if($text) {
    $class = 'notice notice-warning is-dismissible';
    $message = __( $text, 'tutsu_social_review' );
    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
  }
}
add_action( 'admin_notices', 'sr_admin_notice__error' );


// UPDATE POSTS USING WP-CRON
add_action( 'sr_update_social_reviews_hook', 'sr_update_social_reviews' );

wp_next_scheduled( 'sr_update_social_reviews_hook' );

if ( ! wp_next_scheduled( 'sr_update_social_reviews_hook' ) ) {
  wp_schedule_event( time(), 'hourly', 'sr_update_social_reviews_hook' );
}

register_deactivation_hook( __FILE__, 'sr_update_social_reviews_hook_deactivate' );

function sr_update_social_reviews_hook_deactivate() {
   $timestamp = wp_next_scheduled( 'sr_update_social_reviews_hook' );
   wp_unschedule_event( $timestamp, 'sr_update_social_reviews_hook' );
}

add_action('activated_plugin','sr_update_social_reviews');


// // CHECK FOR ERRORS (for testing purpose only)
// add_action('activated_plugin','save_error');
// function save_error(){
//     update_option('plugin_error',  ob_get_contents());
// }

// add_action('deactivated_plugin','unsave_error');
// function unsave_error(){
//     update_option('plugin_error',  '');
// }

// echo get_option('plugin_error');

// add_action('init', 'sr_update_social_reviews');

