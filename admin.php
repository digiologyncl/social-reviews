<?php

function sr_create_social_review() {
  register_post_type( 'social-review',
    array(
      'labels' => array(
        'name' => 'Social Reviews',
        'singular_name' => 'Social Review',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Social Review',
        'edit' => 'Edit',
        'edit_item' => 'Edit Social Review',
        'new_item' => 'New Social Review',
        'view' => 'View',
        'view_item' => 'View Social Review',
        'search_items' => 'Search Social Reviews',
        'not_found' => 'No Social Reviews found',
        'not_found_in_trash' => 'No Social Reviews found in Trash',
        'parent' => 'Parent Social Review'
      ),
      'public' => true,
      'menu_position' => 15,
      'supports' => array( 'title', 'editor' ),
      'taxonomies' => array( '' ),
      'menu_icon' => 'dashicons-thumbs-up',
      'has_archive' => true
    )
  );
}
add_action( 'init', 'sr_create_social_review' );

if( function_exists('acf_add_options_page') ) {
  acf_add_options_sub_page(array(
    'page_title'  => 'Social Reviews Settings',
    'menu_title'  => 'Social Reviews',
    'menu_slug'   => 'social-reviews',
    'capability'  => 'edit_posts',
    'parent_slug' => 'options-general.php',
  ));
}

// ADMIN FIELDS
function sr_add_social_reviews_acf() {
  if( function_exists('acf_add_local_field_group') ) {
//     acf_add_local_field_group(
//       array (
//         'key' => 'group_social_review_facebook_api',
//         'title' => 'Facebook API',
//         'fields' => array (
//           array (
//             'key' => 'field_social_review_facebook_api_id',
//             'label' => 'App ID',
//             'name' => 'sr_facebook_app_id',
//             'type' => 'text',
//             'default_value' => '1648499185168917'
//           ),
//           array (
//             'key' => 'field_social_review_facebook_api_app_secret',
//             'label' => 'App Secret',
//             'name' => 'sr_facebook_app_secret',
//             'type' => 'text',
//             'default_value' => 'b08edc0da3975017987328d835359bba'
//           ),
//           array (
//             'key' => 'field_social_review_facebook_api_page_access_token',
//             'label' => 'Page Access Token',
//             'name' => 'sr_facebook_page_access_token',
//             'type' => 'text',
//             'default_value' => 'EAAXbTSBYMhUBAGGdZCe0iA0tkReWGGU67EJw21FZCLYpoZAdukf1Oo9zE9m0rjetXtBlrrrRF8KYidEv75YGSGpQYP71ZCtY2Nd9ZAEkMfP0whGZCF0bDT00M11yjoZCQwGuG3usG3dEYYkFaGQzHI6J3hboZCKYZBPEZD',
//             'instructions' => '1. Go <a href="https://developers.facebook.com/tools/explorer/?method=GET&path=me%3Ffields%3Did%2Cname%2Cratings&version=v2.9" target="_blank">here</a><br>2. Click on "Get Token"<br>3. Click on "Get Page Access Token<br>4. Select your page<br>5. Copy and past Access Token and Page ID'
//           ),
//           array (
//             'key' => 'field_social_review_facebook_api_page_id',
//             'label' => 'Page ID',
//             'name' => 'sr_facebook_page_id',
//             'type' => 'text',
//             'instructions' => '<a href="https://findmyfbid.com/" target="_blank">Find Page ID</a>'
//           ),
//           array (
//             'key' => 'field_social_review_facebook_url',
//             'label' => 'URL',
//             'name' => 'sr_facebook_url',
//             'type' => 'url',
//           ),
//         ),
//         'location' => array (
//           array (
//             array (
//               'param' => 'options_page',
//               'operator' => '==',
//               'value' => 'social-reviews',
//             ),
//           ),
//         ),
//         'menu_order' => 3,
//         'position' => 'normal',
//         'style' => 'default',
//         'label_placement' => 'top',
//         'instruction_placement' => 'label',
//         'active' => 1,
//       )
//     );

    acf_add_local_field_group(
      array (
        'key' => 'group_google_api',
        'title' => 'Google API',
        'fields' => array (
          array (
            'key' => 'field_google_api_key',
            'label' => 'Google API key',
            'name' => 'sr_google_api_key',
            'type' => 'text',
            'default_value' => 'AIzaSyDkA66x5TDacayQztQdJvXLrMjZ1myEa7s'
          ),
          array (
            'key' => 'field_google_place_id',
            'label' => 'Place ID',
            'name' => 'sr_google_place_id',
            'type' => 'text',
            'instructions' => '<a href="https://developers.google.com/maps/documentation/javascript/examples/places-placeid-finder" target="_blank">Find Place ID</a>'
          ),
          array (
            'key' => 'field_social_review_google_url',
            'label' => 'URL',
            'name' => 'sr_google_url',
            'type' => 'url',
          ),
        ),
        'location' => array (
          array (
            array (
              'param' => 'options_page',
              'operator' => '==',
              'value' => 'social-reviews',
            ),
          ),
        ),
        'menu_order' => 4,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'active' => 1,
      )
    );

    acf_add_local_field_group(
      array (
        'key' => 'group_social_review_yelp_api',
        'title' => 'Yelp API',
        'fields' => array (
          array (
            'key' => 'field_social_review_yelp_api_client_id',
            'label' => 'Client ID',
            'name' => 'sr_yelp_client_id',
            'default_value' => 'NtM1afz3LNwtf5nKrX8znQ',
            'type' => 'text',
          ),
          array (
            'key' => 'field_social_review_yelp_api_client_secret',
            'label' => 'Client Secret',
            'name' => 'sr_yelp_client_secret',
            'default_value' => 'BcaysrvdRwivvrSVYPIG0bQ6Pd1YZEDPvtpVqB5tdsmP3iRvM4ujogrsiTl7EoTZ',
            'type' => 'text',
          ),
          array (
            'key' => 'field_social_review_yelp_api_business_id',
            'label' => 'Yelp Business ID',
            'name' => 'sr_yelp_business_id',
            'type' => 'text',
          ),
          array (
            'key' => 'field_social_review_yelp_url',
            'label' => 'URL',
            'name' => 'sr_yelp_url',
            'type' => 'url',
          ),
        ),
        'location' => array (
          array (
            array (
              'param' => 'options_page',
              'operator' => '==',
              'value' => 'social-reviews',
            ),
          ),
        ),
        'menu_order' => 6,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'active' => 1,
      )
    );

    acf_add_local_field_group(
      array (
        'key' => 'group_social_review_rateabiz_api',
        'title' => 'Rateabiz API',
        'fields' => array (
          array (
            'key' => 'field_social_review_rateabiz_api_id',
            'label' => 'ID',
            'name' => 'sr_rateabiz_id',
            'type' => 'text',
          ),
          array (
            'key' => 'field_social_review_rateabiz_url',
            'label' => 'URL',
            'name' => 'sr_rateabiz_url',
            'type' => 'url',
          ),
        ),
        'location' => array (
          array (
            array (
              'param' => 'options_page',
              'operator' => '==',
              'value' => 'social-reviews',
            ),
          ),
        ),
        'menu_order' => 7,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'active' => 1,
      )
    );

    acf_add_local_field_group(
      array (
        'key' => 'group_social_review_review',
        'title' => 'Review',
        'fields' => array (
          array (
            'key' => 'field_social_review_review_id',
            'label' => 'ID',
            'name' => 'id',
            'type' => 'text',
          ),
          array (
            'key' => 'field_social_review_review_rating',
            'min' => 0,
            'max' => 5,
            'label' => 'Rating',
            'name' => 'rating',
            'type' => 'number',
          ),
          array (
            'key' => 'field_social_review_review_user_name',
            'label' => 'User Name',
            'name' => 'user_name',
            'type' => 'text',
          ),
          array (
            'multiple' => 0,
            'allow_null' => 0,
            'choices' => array (
              // 'custom' => 'Custom',
              // 'facebook' => 'Facebook',
              'google_plus' => 'Google Plus',
              'rateabiz' => 'Rateabiz',
              'yelp' => 'Yelp'
            ),
            'default_value' => array (
              0 => 'custom',
            ),
            'ui' => 0,
            'ajax' => 0,
            'return_format' => 'value',
            'key' => 'field_social_review_review_source',
            'label' => 'Source',
            'name' => 'source',
            'type' => 'select',
          ),
          array (
            'key' => 'field_social_review_overall_rating',
            'label' => 'Overall Rating',
            'name' => 'overall_rating',
            'type' => 'number',
          ),
          array (
            'key' => 'field_social_review_rating_count',
            'label' => 'Rating Count',
            'name' => 'rating_count',
            'type' => 'number',
          ),
        ),
        'location' => array (
          array (
            array (
              'param' => 'post_type',
              'operator' => '==',
              'value' => 'social-review',
            ),
          ),
        ),
        'menu_order' => 5,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'active' => 1,
      )
    );
  };
}
add_action( 'init', 'sr_add_social_reviews_acf' );