<?php

$SR_RATEABIZ_ID = get_field('sr_rateabiz_id', 'options');

function sr_get_social_reviews_rateabiz(){

  $id = $GLOBALS['SR_RATEABIZ_ID'];

  $url = 'http://s3.amazonaws.com/cdn.rateabiz.com/reviews/' . $id . '/reviews.json';

  if($id) {

    $result = json_decode( file_get_contents($url) ) ;


    $reviews = new stdClass();
    $reviews->source = 'rateabiz';

    if($result->reviews){

      $i = 0;

      foreach( $result->reviews as $review ){

        $i++;

        $new_review['id'] = $review->id;
        $new_review['text'] = $review->text;
        $new_review['rating'] = $review->rating;
        $fname = ucwords($review->authorFirstName);
        $new_review['user_name'] = $fname;
        $created_date = $review->createdDate->day . '-' . $review->createdDate->month . '-' . $review->createdDate->year;
        $created_date = strtotime($created_date);
        $new_review['time_created'] = date('Y-m-d H:i:s', $created_date);

        if($i <= 20){
          $all_reviews[] = $new_review;
        }
      }

      $reviews->reviews =  $all_reviews;

      $reviews->overall_rating = $result->stats->averageRating;
      $reviews->rating_count = $result->stats->count;

      return $reviews;

    }

  }

}

