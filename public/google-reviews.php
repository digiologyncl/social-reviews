<?php


$SR_GOOGLE_API_KEY = get_field('sr_google_api_key', 'options');
$SR_GOOGLE_PLACE_ID = get_field('sr_google_place_id', 'options');

function sr_get_social_reviews_google() {

  // https://maps.googleapis.com/maps/api/place/details/json?placeid=PLACE_ID&key=API_KEY

  $url = "https://maps.googleapis.com/maps/api/place/details/json?";

  $api_key = $GLOBALS['SR_GOOGLE_API_KEY'];
  $place_id = $GLOBALS['SR_GOOGLE_PLACE_ID'];

  if($api_key && $place_id) {
    $params = array(
      "placeid" => $place_id,
      "key" => $api_key
    );

    $url .= http_build_query($params);
    $result = json_decode( file_get_contents($url) ) ;

    $reviews = new stdClass();
    $reviews->source = 'google_plus';

    if($result->result->reviews){

      foreach( $result->result->reviews as $review ){

        $new_review['id'] = date("YmdHis", $review->time);
        $new_review['text'] = $review->text;
        $new_review['rating'] = $review->rating;
        $new_review['user_name'] = ucwords($review->author_name);
        $new_review['time_created'] = date("Y-m-d H:i:s", $review->time);

        $all_reviews[] = $new_review;

      }

      $reviews->reviews =  $all_reviews;

      $reviews->overall_rating = $result->result->rating;
      $reviews->rating_count = false;

      return $reviews;

    }
  }

}