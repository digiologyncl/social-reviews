<?php

function sr_show_social_reviews( $atts ){

  $defaults = array(
    'source' => '',
    'slider' => 1 // list or card
  );

  $atts = wp_parse_args( $atts, $defaults );

  $source = $atts['source'];
  $slider = $atts['slider'];


  if( function_exists( 'get_social_reviews_'. $source ) ) {
    call_user_func( 'get_social_reviews_'. $source );
  }

  $args = array(
    'post_type' => 'social-review',
    'meta_key'    => 'source',
    'meta_value'  => $source,
    'posts_per_page' => 1
  );

  // the query
  $the_query = new WP_Query( $args );

  // var
  $overall_rating = 0;
  $rating_count = 0;
  $url = '';

  if($source == 'facebook'){
    $url = get_field('sr_facebook_url', 'options');
  } elseif ($source == 'yelp') {
    $url = get_field('sr_yelp_url', 'options');
  } elseif ($source == 'google_plus') {
    $url = get_field('sr_google_url', 'options');
  } elseif ($source == 'rateabiz') {
    $url = get_field('sr_rateabiz_url', 'options');
  } else {
    $url = '';
  }

  if ( $the_query->have_posts() ) :
    while ( $the_query->have_posts() ) : $the_query->the_post();
      $overall_rating = round(get_field('overall_rating'), 2);
      $rating_count = get_field('rating_count');
    endwhile;
  endif;

  $output = '';

  $output .= '<div class="social-reviews-wrapper social-reviews-' . $source . '">';
  $output .= '<div class="social-reviews-inner">';

  $output .= '<div class="social-reviews-header">';
  $output .= '<div class="social-reviews-header-icon-wrapper">';
  if($url) {
    $output .= '<a href="' . $url . '" target="_blank">';
  }
  $output .= '<div class="social-reviews-header-icon"></div>';
  if($url) {
    $output .= '</a>';
  }
  $output .= '</div>';
  if($rating_count || $overall_rating) {
    $output .= '<div class="social-reviews-header-meta">';
    if($rating_count) {
      $output .= '<span class="social-reviews-header-meta-total-label">Total Reviews:</span> <span class="social-reviews-header-meta-total">' . $rating_count . '</span>';
      $output .= '<span class="social-reviews-header-meta-divider">|</span>';
    }
    if($overall_rating) {
      $output .= '<span class="social-reviews-header-meta-overallrating-label">Overall Rating:</span> <span class="social-reviews-header-meta-overallrating">' . $overall_rating . '/5</span>';
    }
    $output .= '</div>';
  }
  $output .= '</div>';

  $output .= '<div class="social-reviews-body">';
  $output .= '<div class="social-reviews-body-inner">';

  $args = array(
    'post_type' => 'social-review',
    'meta_key'    => 'source',
    'meta_value'  => $source,
    'posts_per_page' => 20
  );

  // the query
  $the_query = new WP_Query( $args );

  if ( $the_query->have_posts() ) {

    if($slider){
      $output .= '<div class="social-reviews-slider">';
    }

    while ( $the_query->have_posts() ) : $the_query->the_post();

      // vars
      $text = get_the_content();
      $author = get_field('user_name');
      $date = get_the_date();
      $rating = get_field('rating');

      if($slider){
        $output .= '<div>';
      }
      $output .= '<div class="social-reviews-review">';
      $output .= '<div class="social-reviews-review-rating">';
      if($rating >= 1){
        $output .= '<span class="social-reviews-review-rating-star"></span>';
      }
      if($rating >= 1.5){
        $output .= '<span class="social-reviews-review-rating-star"></span>';
      }
      if($rating >= 2.5){
        $output .= '<span class="social-reviews-review-rating-star"></span>';
      }
      if($rating >= 3.5){
        $output .= '<span class="social-reviews-review-rating-star"></span>';
      }
      if($rating >= 4.5){
        $output .= '<span class="social-reviews-review-rating-star"></span>';
      }
      $output .= '</div>';
      $output .= '<div class="social-reviews-review-text">';
      if($slider){
        $output .= '<div class="social-reviews-review-text-inner">' . mb_strimwidth($text, 0, 200, "...") . '</div>';
      } else {
        $output .= '<div class="social-reviews-review-text-inner">' . $text . '</div>';
      }
      $output .= '</div>';
      $output .= '<div class="social-reviews-review-meta">';
      $output .= '<div class="social-reviews-review-meta-inner">';
      $output .= '<div class="social-reviews-review-meta-author">' . $author . '</div>';
      $output .= '<div class="social-reviews-review-meta-date">' . $date . '</div>';
      $output .= '</div>';
      $output .= '</div>';
      $output .= '</div>';
      if($slider){
        $output .= '</div>';
      }

    endwhile;

    wp_reset_postdata();

    if($slider){
      $output .= '</div>';
    }

  } else {

    $output .= '<div class="social-reviews-noreviews">Sorry, no reviews to show yet...</div>';

  }

  $output .= '</div>';
  $output .= '</div>';

  if($url) {
    $output .= '<div class="social-reviews-footer">';
    $output .= '<div class="social-reviews-permalink"><a href="' . $url . '" target="_blank">View all reviews</a></div>';
    $output .= '</div>';
  }

  $output .= '</div>';
  $output .= '</div>';

  if($source){
    return $output;
  }

}
add_shortcode( 'social_reviews', 'sr_show_social_reviews' );