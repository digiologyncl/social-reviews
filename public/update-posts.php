<?php

function sr_update_social_reviews_posts( $reviews ) {

  foreach( $reviews->reviews as $review ) {
    // check if review already exists in databse
    $dbreview = get_posts(array(
      'numberposts' => 1,
      'post_type'   => 'social-review',
      'meta_key'    => 'id',
      'meta_value'  => $review['id']
    ));

    // insert post if it does not exist, else update existing post
    if( !isset( $dbreview[0] ) ) {

      // Create post object
      $review_post = array(
        'post_title'    => wp_strip_all_tags( ucwords(str_replace('_', ' ', $reviews->source)) . ' - ' . $review['user_name'] ),// user_name
        'post_content'  => $review['text'],
        'post_status'   => 'publish',
        'post_type'     => 'social-review',
        'post_author'   => 1,
        'post_date'     => $review['time_created']
      );

      // Insert the post into the database
      $new_post_id = wp_insert_post( $review_post );

      // update ACF fields
      update_field( 'source_overall_rating', $reviews->overall_rating, $new_post_id );
      update_field( 'id', $review['id'], $new_post_id );
      update_field( 'rating', $review['rating'], $new_post_id );
      update_field( 'user_name', $review['user_name'] , $new_post_id );
      update_field( 'source', $reviews->source , $new_post_id );
      update_field( 'overall_rating', $reviews->overall_rating , $new_post_id );
      update_field( 'rating_count', $reviews->rating_count , $new_post_id );

    } else {

      // update existing post
      $existing_post_id = $dbreview[0]->ID;

      // Create post object
      $review_post = array(
        'ID'              => $existing_post_id,
        'post_title'    => wp_strip_all_tags( ucwords(str_replace('_', ' ', $reviews->source)) . ' - ' . $review['user_name'] ),// user_name
        'post_content'    => $review['text'],
        'post_type'       => 'social-review',
        'post_author'  => 1
      );

      // Update the post into the database
      wp_update_post( $review_post );

      // update ACF fields
      update_field( 'source_overall_rating', $reviews->overall_rating, $existing_post_id );
      update_field( 'id', $review['id'], $existing_post_id );
      update_field( 'rating', $review['rating'], $existing_post_id );
      update_field( 'user_name', $review['user_name'] , $existing_post_id );
      update_field( 'source', $reviews->source , $existing_post_id );
      update_field( 'overall_rating', $reviews->overall_rating , $existing_post_id );
      update_field( 'rating_count', $reviews->rating_count , $existing_post_id );

    }
  }
}

// fetch social reviews from social networks and update reviews in database
function sr_update_social_reviews() {

  if(!empty($GLOBALS['SR_YELP_BUSINESS_ID'])) {
    $yelp_reviews = sr_get_social_reviews_yelp();
    if( $yelp_reviews->reviews ) sr_update_social_reviews_posts( $yelp_reviews );
  }

  if(!empty($GLOBALS['SR_GOOGLE_API_KEY']) && !empty($GLOBALS['SR_GOOGLE_PLACE_ID'])) {
    $google_reviews = sr_get_social_reviews_google();
    if( $google_reviews->reviews ) sr_update_social_reviews_posts( $google_reviews );
  }

  if(!empty($GLOBALS['SR_RATEABIZ_ID'])) {
    $rateabiz_reviews = sr_get_social_reviews_rateabiz();
    if( $rateabiz_reviews->reviews ) sr_update_social_reviews_posts( $rateabiz_reviews );
  }

  // $facebook_reviews = sr_get_social_reviews_facebook();
  // if( !empty($facebook_reviews->reviews) ) sr_update_social_reviews_posts( $facebook_reviews );

}