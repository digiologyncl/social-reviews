<?php

function sr_enqueue_scripts() {
  // Plugin stylesheet.
  wp_enqueue_style( 'social-reviews-style', sr_social_reviews_plugin_url( 'public/assets/css/style.css' ), array(), null );

  // Plugin javascript.
  wp_enqueue_script( 'social-reviews-js', sr_social_reviews_plugin_url( 'public/assets/js/script.js' ), array( 'jquery' ), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'sr_enqueue_scripts', 98 );