<?php

// GET https://api.yelp.com/v3/businesses/{id}/reviews

/**
 * Please refer to http://www.yelp.com/developers/v3/documentation
 * for the API documentation.
 */

$SR_YELP_CLIENT_ID = get_field('sr_yelp_client_id', 'options');
$SR_YELP_CLIENT_SECRET = get_field('sr_yelp_client_secret', 'options');
$SR_YELP_BUSINESS_ID = get_field('sr_yelp_business_id', 'options');

if(!$SR_YELP_CLIENT_ID) {
  $SR_YELP_CLIENT_ID = 'NtM1afz3LNwtf5nKrX8znQ';
}
if(!$SR_YELP_CLIENT_SECRET) {
  $SR_YELP_CLIENT_SECRET = 'BcaysrvdRwivvrSVYPIG0bQ6Pd1YZEDPvtpVqB5tdsmP3iRvM4ujogrsiTl7EoTZ';
}

// API constants, you shouldn't have to change these.
$SR_YELP_API_HOST = "https://api.yelp.com";
$SR_YELP_REVIEWS_PATH = "/v3/businesses/" . $GLOBALS['SR_YELP_BUSINESS_ID'] . "/reviews";
$SR_YELP_TOKEN_PATH = "/oauth2/token";
$SR_YELP_GRANT_TYPE = "client_credentials";

/**
 * Given a bearer token, send a GET request to the API.
 *
 * @return OAuth bearer token, obtained using client_id and client_secret.
 */

function sr_yelp_obtain_bearer_token() {
  try {
    # Using the built-in cURL library for easiest installation.
    # Extension library HttpRequest would also work here.
    $curl = curl_init();
    if (FALSE === $curl)
      throw new Exception('Failed to initialize');
      $postfields = "client_id=" . $GLOBALS['SR_YELP_CLIENT_ID'] .
      "&client_secret=" . $GLOBALS['SR_YELP_CLIENT_SECRET'] .
      "&grant_type=" . $GLOBALS['SR_YELP_GRANT_TYPE'];
      curl_setopt_array($curl, array(
        CURLOPT_URL => $GLOBALS['SR_YELP_API_HOST'] . $GLOBALS['SR_YELP_TOKEN_PATH'],
        CURLOPT_RETURNTRANSFER => true,  // Capture response.
        CURLOPT_ENCODING => "",  // Accept gzip/deflate/whatever.
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $postfields,
        CURLOPT_HTTPHEADER => array(
          "cache-control: no-cache",
          "content-type: application/x-www-form-urlencoded",
        ),
      ));
      $response = curl_exec($curl);
      if (FALSE === $response)
      throw new Exception(curl_error($curl), curl_errno($curl));
      $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      if (200 != $http_status)
      throw new Exception($response, $http_status);
      curl_close($curl);
    } catch(Exception $e) {
      sr_admin_notice__error(sprintf('Curl failed with error #%d: %s', $e->getCode(), $e->getMessage()));
    }
  $body = json_decode($response);
  $bearer_token = $body->access_token;
  return $bearer_token;
}
/**
 * Makes a request to the Yelp API and returns the response
 *
 * @param    $bearer_token   API bearer token from sr_yelp_obtain_bearer_token
 * @param    $host    The domain host of the API
 * @param    $path    The path of the API after the domain.
 * @param    $url_params    Array of query-string parameters.
 * @return   The JSON response from the request
 */
function sr_yelp_request($bearer_token, $host, $path, $url_params = array()) {
  // Send Yelp API Call
  try {
      $curl = curl_init();
      if (FALSE === $curl)
          throw new Exception('Failed to initialize');
      $url = $host . $path . "?" . http_build_query($url_params);
      curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,  // Capture response.
          CURLOPT_ENCODING => "",  // Accept gzip/deflate/whatever.
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
              "authorization: Bearer " . $bearer_token,
              "cache-control: no-cache",
          ),
      ));
      $response = curl_exec($curl);
      if (FALSE === $response)
          throw new Exception(curl_error($curl), curl_errno($curl));
      $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      if (200 != $http_status)
          throw new Exception($response, $http_status);
      curl_close($curl);
  } catch(Exception $e) {
      sr_admin_notice__error(sprintf('Curl failed with error #%d: %s', $e->getCode(), $e->getMessage()));
  }
  return $response;
}
/**
 * Query the Business API reviews
 *
 * @param    $bearer_token   API bearer token from sr_yelp_obtain_bearer_token
 * @return   The JSON response from the request
 */
function sr_yelp_get_business_reviews($bearer_token) {
  return sr_yelp_request($bearer_token, $GLOBALS['SR_YELP_API_HOST'], $GLOBALS['SR_YELP_REVIEWS_PATH']);
}
/**
 * Queries the API by the input values from the user
 */
function sr_yelp_query_api() {
  $bearer_token = sr_yelp_obtain_bearer_token();

  $response = sr_yelp_get_business_reviews($bearer_token);

  $pretty_response = json_encode(json_decode($response), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

  return json_decode($pretty_response, true);
}

function sr_get_social_reviews_yelp() {
  $response = sr_yelp_query_api();

  $reviews = new stdClass();
  $reviews->source = 'yelp';

  $allreviews = $response['reviews'];

  if($allreviews) {

    foreach( $allreviews as $review ) {

      $new_review['id'] = date("YmdHis", strtotime($review['time_created']));
      $new_review['text'] = $review['text'];
      $new_review['rating'] = $review['rating'];
      $new_review['user_name'] = ucwords($review['user']['name']);
      $new_review['time_created'] = date("Y-m-d H:i:s", strtotime($review['time_created']));

      $all_reviews[] = $new_review;

    }

    $reviews->reviews = $all_reviews;

    // $reviews->overall_rating = $response['rating'];
    $reviews->overall_rating = 5;
    $reviews->rating_count = $response['total'];

    return $reviews;

  }

}