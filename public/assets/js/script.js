(function($) {
  $('.social-reviews-slider').slick({
    fade: true,
    arrows: true,
    dots: false,
    autoplay: true,
    pauseOnHover: true,
    pauseOnFocus: false,
    autoplaySpeed: 4000,
    draggable: false
  });

})( jQuery );
