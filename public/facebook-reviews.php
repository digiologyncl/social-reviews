<?php

function sr_get_social_reviews_facebook(){

  require_once TUTSUSOCIALREVIEW_PLUGIN_DIR . '/src/Facebook/autoload.php';

  $app_id = get_field('sr_facebook_app_id', 'options');
  $app_secret = get_field('sr_facebook_app_secret', 'options');
  $access_token = get_field('sr_facebook_page_access_token', 'options');
  $page_id = get_field('sr_facebook_page_id', 'options');

  if($app_id && $app_secret && $access_token && $page_id) :

  $fb = new \Facebook\Facebook([
    'app_id' => $app_id,
    'app_secret' => $app_secret,
    'default_graph_version' => 'v2.9',
  ]);

  $fb->setDefaultAccessToken($access_token);

  $response = $fb->get('/'.$page_id.'/?fields=overall_star_rating,rating_count,ratings');

  $response = $response->getDecodedBody();

  $reviews = new stdClass();
  $reviews->source = 'facebook';


  $allreviews = $response['ratings']['data'];
  if (empty($allreviews)) $allreviews = '';


  if($allreviews) {

    foreach( $allreviews as $review ){

      $new_review = '';

      if(!empty($review['reviewer']['id'])){
        $new_review['id'] = $review['reviewer']['id'];
      }
      if(!empty($review['review_text'])){
        $new_review['text'] = $review['review_text'];
      }
      if(!empty($review['rating'])){
        $new_review['rating'] = $review['rating'];
      }
      if(!empty($review['reviewer']['name'])){
        $new_review['user_name'] = ucwords($review['reviewer']['name']);
      }
      if(!empty($review['created_time'])){
        $new_review['time_created'] = date("Y-m-d H:i:s", strtotime( $review['created_time'] ));
      }

      if(!empty($review['review_text']) && !empty($review['reviewer']['name'])){
        $all_reviews[] = $new_review;
      }

    }

    $reviews->reviews = $all_reviews;

  }
  $reviews->overall_rating = $response['overall_star_rating'];
  $reviews->rating_count = $response['rating_count'];

  return $reviews;

  endif;

}

